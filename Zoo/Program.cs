﻿using Zoo;

namespace Zoo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Animal[] animals = new Animal[3];
            animals[0] = new Elephant("Grayson", 10);
            animals[1] = new Giraffe("Strech", 2);
            animals[2] = new Lion("Mufasa", 15);

            foreach (var animal in animals) {
                if (animal != null) {
                    animal.DisplayInfo();
                    string huntingBehavior = "";
                    string animalName = "";
                    string animalType = "";
                    Lion? lion = animal as Lion;
                    Elephant? elephant = animal as Elephant;
                    Giraffe? giraffe = animal as Giraffe;
/*                    Lion? lion = (Lion)animal;*/
                    if (lion != null) {
                        animalName = lion.Name;
                        animalType = "lion";
                        huntingBehavior = lion.Hunt();
                    }
                    if (elephant != null)
                    {
                        animalName = elephant.Name;
                        animalType = "elephant";
                        huntingBehavior = elephant.Graze();
                    }
                    if (giraffe != null)
                    {
                        animalName = giraffe.Name;
                            animalType = "giraffe";
                        huntingBehavior = giraffe.Graze();
                    }
                    Console.WriteLine($"{animalName} the {animalType} has the following hunting behavior: {huntingBehavior}");
                    Console.WriteLine();

                }
            }
        }
    }
}