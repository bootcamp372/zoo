﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Zoo
{
    public abstract class Animal
    {
    /*        An abstract property Name of type string.
    • An abstract property Age of type int.
    • An abstract method MakeSound() that returns a string.
    • A non-abstract method DisplayInfo() that prints the animal's name, age,
    and the sound it makes.*/
        
        public string Name { get; set; }
        public int Age { get; set; }

        public Animal(string name, int age)
        {
            Name = name;
            Age = age;
        }
        public abstract string MakeSound();

        public void DisplayInfo()
        {

            Console.WriteLine("Animal: {0}", Name);
            Console.WriteLine("Age: {0}", Age);
            Console.WriteLine("Sound: {0}", MakeSound());
        }
    }

    public interface ICarnivore
    {
        public string Hunt();
    }

    public interface IHerbivore
    {
        public string Graze();
    }
    public class Lion : Animal, ICarnivore
    {
        public Lion(string name, int age) : base(name, age)
        {
            Name = name;
            Age = age;
        }
        public string Hunt()
        {
            string behavior = "The lion stalks its prey and pounces to catch it.";
            return behavior;
        }       
        public override string MakeSound()
        {
            string sound = "roar"; 
            return sound;
        }
    }

    public class Elephant : Animal, IHerbivore
    {
        
        public Elephant(string name, int age) : base(name, age)
        {
            Name = name;
            Age = age;
        }
        public string Graze()
        {
            string behavior = "The elephant uses its trunk to pull leaves from branches.";           
            return behavior;
            
        }
        public override string MakeSound()
        {
            string sound = "trumpet";
            return sound;
        }
    }

    public class Giraffe : Animal, IHerbivore
    {
        public Giraffe(string name, int age) : base(name, age)
        {
            Name = name;
            Age = age;
        }

        public string Graze()
        {
            string behavior = "The giraffe streches its long neck to pull leaves from trees.";
            return behavior;
        }

        public override sealed string MakeSound()
        {
            string sound = "hum";
            return sound;
        }
    }
}
